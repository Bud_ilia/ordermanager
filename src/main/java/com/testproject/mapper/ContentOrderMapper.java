package com.testproject.mapper;

import com.testproject.dto.ContentOrderDTO;
import com.testproject.entities.ContentOrder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ContentOrderMapper {
    ContentOrder toEntity(ContentOrderDTO dto);

    ContentOrderDTO toDto(ContentOrder entity);

    @Mapping(target = "id")
    ContentOrder update(ContentOrderDTO dto, @MappingTarget() ContentOrder entity);
}
