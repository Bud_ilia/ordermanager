package com.testproject.mapper;

import com.testproject.dto.OrderDTO;
import com.testproject.entities.Ordering;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        uses = {ContentOrderMapper.class})
public interface OrderMapping {
    Ordering toEntity(OrderDTO dto);

    OrderDTO toDto(Ordering entity);

    @Mapping(target = "id")
    Ordering update(OrderDTO dto, @MappingTarget() Ordering entity);
}
