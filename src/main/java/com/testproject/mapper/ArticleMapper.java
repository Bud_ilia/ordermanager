package com.testproject.mapper;

import com.testproject.dto.ArticleDTO;
import com.testproject.entities.Article;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ArticleMapper {
    Article toEntity(ArticleDTO dto);

    ArticleDTO toDto(Article entity);

    @Mapping(target = "id")
    Article update(ArticleDTO dto, @MappingTarget() Article entity);
}
