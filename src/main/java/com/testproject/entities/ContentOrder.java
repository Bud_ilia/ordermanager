package com.testproject.entities;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"article", "ordering"})
public class ContentOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer count;
    private BigDecimal cost;

    @ManyToOne(optional = false)
    @JoinColumn
    private Article article;

    @ManyToOne(optional = false)
    @JoinColumn
    private Ordering ordering;

}
