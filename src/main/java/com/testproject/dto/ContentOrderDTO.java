package com.testproject.dto;

import lombok.*;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContentOrderDTO {
    private Long id;
    private ArticleDTO article;
    private BigDecimal cost;
    private Integer count;

}
