package com.testproject.controllers;

import com.testproject.dto.OrderDTO;
import com.testproject.services.OrderingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping(value = "/api/ordering/")
public class OrderingController {

    private final OrderingService service;

    @Autowired
    public OrderingController(OrderingService service) {

        this.service = service;
    }

    @DeleteMapping("/deleted/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        boolean res = service.delete(id);
        if (res)
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body("Запись удалена");
        else
            return ResponseEntity.status(HttpStatus.GONE)
                    .body("Запись не удалена");
    }

    @DeleteMapping("/all-orders")
    public ResponseEntity<String> deleteAllOrders() {
        boolean res = service.deleteAllOrders();
        if (res)
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body("Запись удалена");
        else
            return ResponseEntity.status(HttpStatus.GONE)
                    .body("Запись не удалена");
    }

    @DeleteMapping("/all-db")
    public ResponseEntity<String> cleanDB() {
        boolean res = service.cleanDB();
        if (res)
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body("Запись удалена");
        else
            return ResponseEntity.status(HttpStatus.GONE)
                    .body("Запись не удалена");
    }

    @GetMapping("/element/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderDTO getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @GetMapping("/list-ordering-email")
    @ResponseStatus(HttpStatus.OK)
    List<OrderDTO> getByEmail(@Param("emailStr") String emailStr) {
        return service.getByEmail(emailStr);
    }

    @GetMapping("/list")
    @ResponseStatus(HttpStatus.OK)
    public List<OrderDTO> getAll() {
        return service.getAll();
    }

    @GetMapping("/{filename}")
    @ResponseStatus(HttpStatus.OK)
    public void FileDownload(@PathVariable String filename, HttpServletResponse response) {
        String[] name = filename.split("\\.");
        String way = "";
        if (name.length != 0)
            way = System.getProperty("user.home") + File.separator + name[0] + ".xml";
        else way = System.getProperty("user.home") + File.separator + filename + ".xml";
        service.write(way);
        Path file = Paths.get(way);
        try {
            Files.copy(file, response.getOutputStream());
            response.getOutputStream().flush();
        } catch (IOException e) {
            e.getMessage();
        }
    }

    @PostMapping(value = "file", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<String> uploadFile(MultipartFile file) {
        String name = file.getOriginalFilename();
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File(name)));
                stream.write(bytes);
                stream.close();
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Вам не удалось загрузить " + name + " => " + e.getMessage());
            }
        } else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Вам не удалось загрузить " + name + " потому что файл пустой.");
        service.read(name);
        return ResponseEntity.ok("Вы удачно загрузили " + name + " в " + name + "-uploaded !");
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OrderDTO create(@RequestBody OrderDTO dto) {
        return service.create(dto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody OrderDTO dto, @PathVariable Long id) {
        service.update(id, dto);
    }
}
