package com.testproject.controllers;

import com.testproject.dto.ArticleDTO;
import com.testproject.services.ArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/api/article")
public class ArticleController {
    private final ArticlesService serviceArticle;

    @Autowired
    public ArticleController(ArticlesService serviceArticle) {
        this.serviceArticle = serviceArticle;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ArticleDTO create(@RequestBody ArticleDTO dto) {
        return serviceArticle.create(dto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody ArticleDTO dto, @PathVariable Long id) {
        serviceArticle.update(id, dto);
    }

    @GetMapping("/element/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ArticleDTO getById(@PathVariable Long id) {
        return serviceArticle.getById(id);
    }

    @GetMapping("/list-price-between")
    @ResponseStatus(HttpStatus.OK)
    List<ArticleDTO> getByPriceBetween(@Param("start") BigDecimal start, @Param("end") BigDecimal end) {
        return serviceArticle.getByPriceBetween(start, end);
    }

    @GetMapping("/list-price-after")
    @ResponseStatus(HttpStatus.OK)
    public List<ArticleDTO> getByPriceAfter(@Param("priceAfter") BigDecimal priceAfter) {
        return serviceArticle.getByPriceAfter(priceAfter);
    }

    @GetMapping("/list-price-before")
    @ResponseStatus(HttpStatus.OK)
    public List<ArticleDTO> getByPriceBefore(@Param("priceBefore") BigDecimal priceBefore) {
        return serviceArticle.getByPriceBefore(priceBefore);
    }

    @GetMapping("/list")
    @ResponseStatus(HttpStatus.OK)
    public List<ArticleDTO> getAll() {
        return serviceArticle.getAll();
    }

    @DeleteMapping("/deleted/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        boolean res = serviceArticle.delete(id);

        if (res)
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body("Запись удалена");
        else
            return ResponseEntity.status(HttpStatus.GONE)
                    .body("Запись не удалена");
    }

    @DeleteMapping
    public ResponseEntity<String> deleteAll() {
        boolean res = serviceArticle.deleteAll();

        if (res)
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body("Записи удалены");
        else
            return ResponseEntity.status(HttpStatus.GONE)
                    .body("Записи не удалены");
    }

    @PostMapping(value = "file", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<String> uploadFile(MultipartFile file) {
        String name = file.getOriginalFilename();
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File(name)));
                stream.write(bytes);
                stream.close();
                serviceArticle.readJackson(name);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body("Вам не удалось загрузить " + name + " => " + e.getMessage());
            }
        } else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Вам не удалось загрузить " + name + " потому что файл пустой.");
        serviceArticle.readJackson(name);
        return ResponseEntity.ok("Вы удачно загрузили " + name + " в " + name + "-uploaded !");
    }

    @GetMapping("/downloaded/{filename}")
    @ResponseStatus(HttpStatus.OK)
    public void FileDownload(@PathVariable String filename, HttpServletResponse response) {
        String[] name = filename.split("\\.");
        String way = "";
        if (name.length != 0)
            way = System.getProperty("user.home") + File.separator + name[0] + ".xml";
        else way = System.getProperty("user.home") + File.separator + filename + ".xml";
        serviceArticle.writeJackson(way);
        Path file = Paths.get(way);
        try {
            Files.copy(file, response.getOutputStream());
            response.getOutputStream().flush();
        } catch (IOException e) {
            e.getMessage();
        }
    }

//    @GetMapping("/jackson/{filename}")
//    @ResponseStatus(HttpStatus.OK)
//    public void FileDownload_2(@PathVariable String filename, HttpServletResponse response) {
//        String[] name = filename.split("\\.");
//        String way = "";
//        if (name.length != 0)
//            way = System.getProperty("user.home") + File.separator + name[0] + ".xml";
//        else way = System.getProperty("user.home") + File.separator + filename + ".xml";
//        serviceArticle.writeJackson(way);
//        Path file = Paths.get(way);
//        try {
//            Files.copy(file, response.getOutputStream());
//            response.getOutputStream().flush();
//        } catch (IOException e) {
//            e.getMessage();
//        }
//    }
}
