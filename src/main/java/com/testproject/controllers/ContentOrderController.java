package com.testproject.controllers;

import com.testproject.dto.ContentOrderDTO;
import com.testproject.services.ContentOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping(value = "/api/content-order")
public class ContentOrderController {

    private final ContentOrderService service;

    @Autowired
    public ContentOrderController(ContentOrderService service) {
        this.service = service;
    }

    @DeleteMapping("/deleted/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {

        boolean res = service.delete(id);
        if (res)
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body("Запись удалена");
        else
            return ResponseEntity.status(HttpStatus.GONE)
                    .body("Запись не удаленв");
    }

    @DeleteMapping
    public ResponseEntity<String> deleteAll() {
        boolean res = service.deleteAll();
        if (res)
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                    .body("Записи удалены");
        else
            return ResponseEntity.status(HttpStatus.GONE)
                    .body("Записи не удалены");
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody ContentOrderDTO dto, @PathVariable Long id) {
        service.update(id, dto);
    }

    @GetMapping("/element/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ContentOrderDTO getById(@PathVariable Long id) {
        return service.findById(id);
    }

    @GetMapping("/list-count-between")
    @ResponseStatus(HttpStatus.OK)
    public List<ContentOrderDTO> getByCountBetween(Integer start, Integer end) {
        return service.findByCountBetween(start, end);
    }

    @GetMapping("/list-count-after")
    @ResponseStatus(HttpStatus.OK)
    public List<ContentOrderDTO> getByCountAfter(Integer countAfter) {
        return service.findByCountAfter(countAfter);
    }

    @GetMapping("/list-count-before")
    public List<ContentOrderDTO> getByCountBefore(Integer countBefore) {
        return service.findByCountBefore(countBefore);
    }

    @GetMapping("/list")
    @ResponseStatus(HttpStatus.OK)
    public List<ContentOrderDTO> getAll() {
        return service.getAll();
    }

    @GetMapping("/{filename}")
    @ResponseStatus(HttpStatus.OK)
    public void FileDownload(@PathVariable String filename, HttpServletResponse response) {
        String[] name = filename.split(".");
        String way;
        if (name.length != 0)
            way = System.getProperty("user.home") + File.separator + name[0] + ".xml";
        else way = System.getProperty("user.home") + File.separator + filename + ".xml";
        service.write(way);
        Path file = Paths.get(way);
        try {
            Files.copy(file, response.getOutputStream());
            response.getOutputStream().flush();
        } catch (IOException e) {
            e.getMessage();
        }
    }
}
