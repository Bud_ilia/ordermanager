package com.testproject.services;

import com.testproject.dto.ContentOrderDTO;
import com.testproject.entities.ContentOrder;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ContentOrderService {
    void update(Long id, ContentOrderDTO contentOrderDTO);

    ContentOrderDTO findById(Long id);

    List<ContentOrderDTO> findByCountBetween(Integer start, Integer end);

    List<ContentOrderDTO> findByCountAfter(Integer countAfter);

    List<ContentOrderDTO> findByCountBefore(Integer countBefore);

    List<ContentOrderDTO> getAll();

    boolean delete(Long id);

    boolean deleteAll();

    void write(String fileName);
}
