package com.testproject.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.testproject.dto.OrderDTO;
import com.testproject.dto.jackson_wrappers.OrderingsWrapper;
import com.testproject.entities.Article;
import com.testproject.entities.ContentOrder;
import com.testproject.entities.Ordering;
import com.testproject.mapper.OrderMapping;
import com.testproject.repositories.ArticleRepository;
import com.testproject.repositories.ContentOrderRepository;
import com.testproject.repositories.OrderRepository;
import com.testproject.services.OrderingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrderingServiceImpl implements OrderingService {
    private final ArticleRepository articleRepository;
    private final ContentOrderRepository contentOrderRepository;
    private final OrderRepository orderRepository;
    private final OrderMapping orderMapping;
    private final ObjectMapper xmlMapper;

    @Autowired
    public OrderingServiceImpl(ArticleRepository articleRepository, ContentOrderRepository contentOrderRepository,
                               OrderRepository orderRepository, OrderMapping orderMapping,
                               @Qualifier("xmlMapper") ObjectMapper xmlMapper) {
        this.articleRepository = articleRepository;
        this.contentOrderRepository = contentOrderRepository;
        this.orderRepository = orderRepository;
        this.orderMapping = orderMapping;
        this.xmlMapper = xmlMapper;
    }

    public OrderDTO getById(Long id) {
        Optional<OrderDTO> dto = orderRepository.findById(id)
                .map(orderMapping::toDto);
        if (dto.isPresent()) {
            log.debug("Запись найдена по id = {}", id);
            return dto.get();
        } else {
            log.debug("Запись не найдена по id = {}", id);
            return null;
        }

    }

    @Override
    public List<OrderDTO> getByEmail(String emailStr) {
        List<OrderDTO> list = orderRepository.findByEmail(emailStr).stream()
                .map(orderMapping::toDto)
                .collect(Collectors.toList());
        log.debug("Записи успешно найдены");
        return list;
    }

    @Override
    public OrderDTO create(OrderDTO orderDTO) {
        Ordering savedEntity = orderRepository.save(Ordering.builder()
                .time(LocalDateTime.now())
                .email(orderDTO.getEmail())
                .build());

        orderDTO.setId(savedEntity.getId());

        List<ContentOrder> contentOrderLis = Optional.ofNullable(orderDTO.getContentOrders())
                .filter(contentOrderDTOList -> !contentOrderDTOList.isEmpty())
                .map(contentOrderDTOList ->
                        contentOrderDTOList.stream().map(contentOrderDTO -> {
                            // по смыслу это справочник и он здесь должен просто браться по id, а не сохраняться
                            Article article = articleRepository.save(Article.builder()
                                    .name(contentOrderDTO.getArticle().getName())
                                    .price(contentOrderDTO.getArticle().getPrice())
                                    .build());
                            return contentOrderRepository.save(ContentOrder.builder()
                                    .count(contentOrderDTO.getCount())
                                    .cost(contentOrderDTO.getCost())
                                    .article(article)
                                    .ordering(savedEntity)
                                    .build());
                        }).collect(Collectors.toList()))
                // что делаем если нет содержимого заказа?
                .orElseThrow(() -> new RuntimeException("Заказ должен быть не пустой"));

        log.debug("Save order record {}", savedEntity);
        return orderDTO;
    }

    @Override
    @Transactional
    public void update(Long id, OrderDTO orderDTO) {
        Ordering ordering = orderRepository.findById(id)
                .map(entity -> orderMapping.update(orderDTO, entity))
                .orElseThrow(() -> new RuntimeException("Order not found with id" + id));

        orderRepository.save(ordering);
        log.debug("update record {}", ordering);

    }

    @Override
    public List<OrderDTO> getAll() {
        List<OrderDTO> list = orderRepository.findAllWithContent()
                .stream()
                .map(orderMapping::toDto)
                .collect(Collectors.toList());
        log.debug("Get all orders: {}", list);
        return list;
    }

    @Override
    @Transactional
    public boolean deleteAllOrders() {
        try {
            orderRepository.deleteAll();
        } catch (Exception e) {
            log.error("While delete orders error occurred", e);
            return false;
        }
        log.debug("deleted all orders");
        return true;
    }

    @Override
    @Transactional
    public boolean cleanDB() {
        try {
            orderRepository.deleteAll();
            articleRepository.deleteAll();
            contentOrderRepository.deleteAll();
        } catch (Exception e) {
            log.error("While delete orders error occurred", e);
            return false;
        }
        log.debug("deleted all orders");
        return true;
    }

    @Override
    @Transactional
    public boolean delete(Long id) {
        Ordering ordering = orderRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Not found order by id " + id));
        try {
            orderRepository.deleteById(id);
        } catch (Exception e) {
            log.error("While delete order error occurred", e);
            return false;
        }
        log.debug("deleted {}", ordering);
        return true;
    }


    //TODO: При десериализации происходит ошибка, если в десериализации учавствует поле "время" в OrderDto
    @Override
    public void read(String fileWay) {
        try {
            XMLInputFactory f = XMLInputFactory.newFactory();
            File file = new File(fileWay);
            OrderingsWrapper wrapper = new OrderingsWrapper();
            XMLStreamReader sr = f.createXMLStreamReader(new FileInputStream(file));
            while (sr.hasNext()) {
                wrapper = xmlMapper.readValue(file, OrderingsWrapper.class);
                sr.next();
            }
            for (OrderDTO ordering : wrapper.getOrderings()) {
                create(ordering);
            }
        } catch (Exception e) {
            log.debug("Произошла ошибка. {}", e.getMessage());
            return;
        }
        log.debug("Все записи прочитаны из xml файл: {}", fileWay);
    }


    @Override
    public void write(String fileName) {
        XmlMapper mapper = new XmlMapper();
        File file = new File(fileName);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            FileOutputStream fos = new FileOutputStream(file);

            OrderingsWrapper wrapper = OrderingsWrapper.builder()
                    .orderings(getAll())
                    .build();
            mapper.writeValue(byteArrayOutputStream, wrapper);
            byteArrayOutputStream.writeTo(fos);
        } catch (IOException e) {
            log.debug("Произошла ошибка. {}", e.getMessage());
        }
        log.debug("Все записи записаны в xml файл: {}", fileName);
    }
}