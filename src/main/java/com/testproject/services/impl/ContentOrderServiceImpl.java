package com.testproject.services.impl;

import com.testproject.dto.ContentOrderDTO;
import com.testproject.entities.ContentOrder;
import com.testproject.mapper.ContentOrderMapper;
import com.testproject.repositories.ArticleRepository;
import com.testproject.repositories.ContentOrderRepository;
import com.testproject.repositories.OrderRepository;
import com.testproject.services.ContentOrderService;
import lombok.extern.slf4j.Slf4j;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ContentOrderServiceImpl implements ContentOrderService {
    private final ArticleRepository articleRepository;
    private final ContentOrderRepository contentOrderRepository;
    private final OrderRepository orderRepository;
    private final ContentOrderMapper contentOrderMapper;

    public ContentOrderServiceImpl(ArticleRepository articleRepository, ContentOrderRepository contentOrderRepository, OrderRepository orderRepository, ContentOrderMapper contentOrderMapper) {
        this.articleRepository = articleRepository;
        this.contentOrderRepository = contentOrderRepository;
        this.orderRepository = orderRepository;
        this.contentOrderMapper = contentOrderMapper;
    }

    @Override
    public void update(Long id, ContentOrderDTO contentOrderDTO) {
        ContentOrder contentOrder = contentOrderRepository.findById(id)
                .map(entity -> contentOrderMapper.update(contentOrderDTO, entity))
                .orElseThrow(() -> new RuntimeException("Order  not found with id" + id));
        contentOrderRepository.save(contentOrder);
        log.debug("updated record {}", contentOrder.toString());
    }

    public ContentOrderDTO findById(Long id) {
        Optional<ContentOrderDTO> dto = contentOrderRepository.findById(id)
                .map(contentOrderMapper::toDto);
        if (dto.isPresent())
            log.debug("Запись найдена по id = {}", id);
        else
            log.debug("Запись найдена по id = {}", id);
        return dto.get();
    }

    @Override
    public List<ContentOrderDTO> findByCountBetween(Integer start, Integer end) {
        List<ContentOrderDTO> list = contentOrderRepository.findByCountBetween(start, end).stream()
                .map(contentOrderMapper::toDto)
                .collect(Collectors.toList());
        String info = "Найдены записи от " + start.toString() + " до " + end.toString();
        log.debug(info);
        return list;
    }

    @Override
    public List<ContentOrderDTO> findByCountAfter(Integer countAfter) {
        List<ContentOrderDTO> list = contentOrderRepository.findByCountAfter(countAfter).stream()
                .map(contentOrderMapper::toDto)
                .collect(Collectors.toList());
        log.debug("Нпйдены записи до {}", countAfter);
        return list;
    }

    @Override
    public List<ContentOrderDTO> findByCountBefore(Integer countBefore) {
        List<ContentOrderDTO> list = contentOrderRepository.findByCountBefore(countBefore).stream()
                .map(contentOrderMapper::toDto)
                .collect(Collectors.toList());
        log.debug("Нпйдены записи после {}", countBefore);
        return list;
    }

    @Override
    public List<ContentOrderDTO> getAll() {
        List<ContentOrderDTO> list = contentOrderRepository.findAll().stream()
                .map(contentOrderMapper::toDto)
                .collect(Collectors.toList());
        log.debug("get all records");
        return list;
    }

    @Override
    public boolean delete(Long id) {
        try {
            contentOrderRepository.deleteById(id);
        } catch (Exception e) {
            log.debug("Запись не удалена. Причина: {}", e.getMessage());
            return false;
        }
        log.debug("deleted content order by id = {}", id);
        return true;
    }

    @Override
    public boolean deleteAll() {
        try {
            contentOrderRepository.deleteAll();
        } catch (Exception e) {
            log.debug("Запись не удалена. Причина: {}", e.getMessage());
            return false;
        }
        log.debug("deleted all records");
        return true;
    }

    @Override
    public void write(String fileName) {
        List<ContentOrderDTO> list = getAll();
        Document doc = new Document();
        doc.setRootElement(new Element("Contents", Namespace.getNamespace("contents")));
        for (ContentOrderDTO contentDTO : list) {
            Element contentElement = new Element("Content", Namespace.getNamespace("contents"));
            contentElement.setAttribute("id", String.valueOf(contentDTO.getId()));

            contentElement.addContent(new Element("Cost",
                    Namespace.getNamespace("contents")).setText("" + contentDTO.getCost().toString()));
            contentElement.addContent(new Element("Count",
                    Namespace.getNamespace("contents")).setText("" + contentDTO.getCount().toString()));

            contentElement.addContent(new Element("ArticleName",
                    Namespace.getNamespace("contents")).setText("" + contentDTO.getArticle().getName()));

            contentElement.addContent(new Element("ArticleCount",
                    Namespace.getNamespace("contents")).setText("" + contentDTO.getCount()));
            contentElement.addContent(new Element("ArticleCount",
                    Namespace.getNamespace("contents")).setText("" + contentDTO.getArticle().getPrice().toString()));

            doc.getRootElement().addContent(contentElement);
        }
        XMLOutputter writer = new XMLOutputter(Format.getPrettyFormat());
        try {
            writer.output(doc, new FileOutputStream(fileName));
            log.debug("Запись успешно завершена");
        } catch (FileNotFoundException e) {
            String info = "Файл не найден " + fileName + ". Причина";
            log.debug(info + ": {}", e.getCause());
        } catch (IOException e) {
            log.debug("В процессе записи произошла ошибка. {}.", e.getMessage());
        }
    }

}
