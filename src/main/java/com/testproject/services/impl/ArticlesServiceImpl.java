package com.testproject.services.impl;


import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.testproject.dto.ArticleDTO;
import com.testproject.dto.jackson_wrappers.ArticlesWrapper;
import com.testproject.entities.Article;
import com.testproject.mapper.ArticleMapper;
import com.testproject.mapper.ContentOrderMapper;
import com.testproject.repositories.ArticleRepository;
import com.testproject.repositories.ContentOrderRepository;
import com.testproject.repositories.OrderRepository;
import com.testproject.services.ArticlesService;
import lombok.extern.slf4j.Slf4j;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ArticlesServiceImpl implements ArticlesService {

    private final ArticleRepository articleRepository;
    private final ArticleMapper articleMapper;
    private final ContentOrderRepository contentOrderRepository;
    private final OrderRepository orderRepository;
    private final ContentOrderMapper contentOrderMapper;

    @Autowired
    public ArticlesServiceImpl(ArticleRepository articleRepository, ArticleMapper articleMapper, ContentOrderRepository contentOrderRepository, OrderRepository orderRepository, ContentOrderMapper contentOrderMapper) {
        this.articleRepository = articleRepository;
        this.articleMapper = articleMapper;
        this.contentOrderRepository = contentOrderRepository;
        this.orderRepository = orderRepository;
        this.contentOrderMapper = contentOrderMapper;
    }

    @Override
    public ArticleDTO create(ArticleDTO articleDTO) {
        Article savedArticle = articleRepository.save(
                Article.builder()
                        .name(articleDTO.getName())
                        .price(articleDTO.getPrice())
                        .build()
        );
        articleDTO.setId(savedArticle.getId());
        log.debug("Save article {}", savedArticle);
        return articleDTO;
    }

    @Override
    public void update(Long id, ArticleDTO articleDTO) {
        Article article = articleRepository.findById(id)
                .map(entity -> articleMapper.update(articleDTO, entity))
                .orElseThrow(() -> new RuntimeException("Article not found with id" + id));

        articleRepository.save(article);
        log.debug("update article {}", article);
    }

    @Override
    public ArticleDTO getById(Long id) {
        ArticleDTO article = articleRepository.findById(id).map(articleMapper::toDto)
                .orElseThrow(() -> new RuntimeException("Article not found by id"));
        if (article != null)
            log.debug("Запись найдена по id = {}", id);
        else
            log.debug("Запись не найдена по id = {}", id);
        return article;
    }

    @Override
    public List<ArticleDTO> getByPriceBetween(BigDecimal start, BigDecimal end) {

        List<ArticleDTO> list = articleRepository.findByPriceBetweenCustom(start, end).stream()
                .map(articleMapper::toDto)
                .collect(Collectors.toList());
        String info = String.format("Найдены записи от %s до $s", start.toString(), end.toString());
        log.debug(info);
        return list;
    }

    @Override
    public List<ArticleDTO> getByPriceAfter(BigDecimal priceAfter) {
        List<ArticleDTO> list = articleRepository.findByPriceAfter(priceAfter).stream()
                .map(articleMapper::toDto)
                .collect(Collectors.toList());
        log.debug("Найдены все записи до {}", priceAfter.toString());
        return list;
    }

    @Override
    public List<ArticleDTO> getByPriceBefore(BigDecimal priceBefore) {
        List<ArticleDTO> list = articleRepository.findByPriceBefore(priceBefore).stream()
                .map(articleMapper::toDto)
                .collect(Collectors.toList());
        log.debug("Найдены записи после {}", priceBefore.toString());
        return list;
    }

    @Override
    public List<ArticleDTO> getAll() {
        List<ArticleDTO> listArticles = articleRepository.findAll().stream()
                .map(articleMapper::toDto)
                .collect(Collectors.toList());
        log.debug("Get all articles {}", listArticles);
        return listArticles;
    }

    @Override
    public boolean delete(Long id) {
        ArticleDTO articleDTO = articleRepository.findById(id).map(articleMapper::toDto)
                .orElseThrow(() -> new RuntimeException("Not found article record"));
        try {
            articleRepository.deleteById(articleDTO.getId());
        } catch (Exception e) {
            log.debug("Запись не удалена: {}", id, e.getCause());
            return false;
        }
        log.debug("deleted content order record {}", id);
        return true;
    }

    @Override
    public boolean deleteAll() {
        try {
            articleRepository.deleteAll();
        } catch (Exception e) {
            log.debug("Запись не удалена: {}", e.getCause());
            return false;
        }
        log.debug("deleted content order record");
        return true;
    }

    @Override
    public void readJackson(String fileWay) {
        try {
            XMLInputFactory f = XMLInputFactory.newFactory();
            File file = new File(fileWay);
            ArticlesWrapper wrapper = new ArticlesWrapper();
            XMLStreamReader sr = f.createXMLStreamReader(new FileInputStream(file));
            XmlMapper xmlMapper = new XmlMapper();
            while (sr.hasNext()) {
                wrapper = xmlMapper.readValue(file, ArticlesWrapper.class);
                sr.next();
            }

            for (ArticleDTO article : wrapper.getArticles()) {
                create(article);
            }
        } catch (Exception e) {
            log.debug("Произошла ошибка. {}", e.getMessage());
        }
        log.debug("Все записи прочитаны из xml файл: {}", fileWay);
    }

    @Override
    public void read(String fileWay) {
        addingRecord(readFromFile(fileWay));
    }


    private List<ArticleDTO> readFromFile(String fileWay) {
        List<ArticleDTO> list = new ArrayList<>();
        SAXBuilder saxBuilder = new SAXBuilder();
        File xmlFile = new File(fileWay);
        try {
            Document document = saxBuilder.build(xmlFile);
            Element rootNode = document.getRootElement();
            List<Element> ArticleList = rootNode.getChildren("Article", Namespace.getNamespace("articles"));
            for (Element element : ArticleList) {
                ArticleDTO article = new ArticleDTO();
                article.setName(
                        element.getChildText("Name", Namespace.getNamespace("articles")));

                if (!element.getChildText("Count", Namespace.getNamespace("articles")).equals("null"))
                    article.setCount(Integer.parseInt(
                            element.getChildText("Count", Namespace.getNamespace("articles"))));
                else article.setCount(null);


                if (!element.getChildText("Price", Namespace.getNamespace("articles")).equals("null"))
                    article.setPrice(BigDecimal.valueOf(Double.parseDouble(
                            element.getChildText("Price", Namespace.getNamespace("articles")))));
                else
                    article.setPrice(null);

                list.add(article);
                log.debug("Получен экземпляр класса Article: {}", article.toString());
            }
            log.debug("Все экземпляры класса Article в списке");
        } catch (JDOMException | IOException e) {
            log.debug("Произошла ошибка: {}", e.getMessage());
        }
        return list;
    }

    @Override
    public void write(String fileName) {
        List<ArticleDTO> list = getAll();
        Document doc = new Document();
        doc.setRootElement(new Element("Articles", Namespace.getNamespace("articles")));
        for (ArticleDTO article : list) {
            Element articleElement = new Element("Article", Namespace.getNamespace("articles"));
            articleElement.setAttribute("id", String.valueOf(article.getId()));

            articleElement.addContent(new Element("Name",
                    Namespace.getNamespace("articles")).setText("" + article.getName()));

            articleElement.addContent(new Element("Price",
                    Namespace.getNamespace("articles")).setText("" + String.valueOf(article.getPrice())));

            articleElement.addContent(new Element("Count",
                    Namespace.getNamespace("articles")).setText("" + String.valueOf(article.getCount())));

            doc.getRootElement().addContent(articleElement);
        }
        XMLOutputter writer = new XMLOutputter(Format.getPrettyFormat());
        try {
            writer.output(doc, new FileOutputStream(fileName));
            log.debug("Запись успешно завершена");
        } catch (FileNotFoundException e) {
            log.debug("Файл не найден {}", fileName);
        } catch (IOException e) {
            log.debug("В процессе записи произошла ошибка. {}.", e.getMessage());
        }
    }

    @Override
    public void writeJackson(String fileName) {
        XmlMapper mapper = new XmlMapper();
        File file = new File(fileName);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ArticlesWrapper wrapper = ArticlesWrapper.builder()
                    .articles(getAll())
                    .build();
            FileOutputStream fos = new FileOutputStream(file);
            mapper.writeValue(byteArrayOutputStream, wrapper);
            byteArrayOutputStream.writeTo(fos);
        } catch (IOException e) {
            log.debug("Произошла ошибка. {}", e.getMessage());
        }
        log.debug("Все записи записаны в xml файл: {}", fileName);
    }

    private List<ArticleDTO> addingRecord(List<ArticleDTO> list) {
        List<ArticleDTO> savedList = new ArrayList<>();
        for (ArticleDTO articleDTO : list) {
            create(articleDTO);
        }
        return savedList;
    }

}
