package com.testproject.services;

import com.testproject.dto.OrderDTO;

import java.util.List;


public interface OrderingService {
    OrderDTO create(OrderDTO orderDTO);

    void update(Long id, OrderDTO orderDTO);

    OrderDTO getById(Long id);

    List<OrderDTO> getByEmail(String emailStr);

    List<OrderDTO> getAll();

    boolean delete(Long id);

    boolean deleteAllOrders();

    boolean cleanDB();

    void read(String fileWay);

    void write(String fileName);
}
