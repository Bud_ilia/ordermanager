package com.testproject.services;


import com.testproject.dto.ArticleDTO;
import com.testproject.entities.Article;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ArticlesService {

    ArticleDTO create(ArticleDTO articleDTO);

    void update(Long id, ArticleDTO articleDTO);

    ArticleDTO getById(Long id);

    List<ArticleDTO> getByPriceBetween(BigDecimal start, BigDecimal end);

    List<ArticleDTO> getByPriceAfter(BigDecimal priceAfter);

    List<ArticleDTO> getByPriceBefore(BigDecimal priceBefore);

    List<ArticleDTO> getAll();

    boolean delete(Long id);

    boolean deleteAll();

    void readJackson(String fileWay);

    void read(String fileWay);

    void writeJackson(String fileName);

    void write(String fileName);
}
