package com.testproject.repositories;

import com.testproject.entities.Ordering;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Ordering, Long>, QuerydslPredicateExecutor<Ordering> {

    @Query("SELECT ordering from Ordering ordering where ordering.email = :emailStr")
    List<Ordering> findByEmail(@Param("emailStr") String emailStr);

    @Query("SELECT ordering from Ordering ordering left join fetch ordering.contentOrders")
    List<Ordering> findAllWithContent();

}
