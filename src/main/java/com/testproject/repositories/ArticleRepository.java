package com.testproject.repositories;

import com.testproject.entities.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long>,
        QuerydslPredicateExecutor<Article> {

    List<Article> findByPriceBetween(BigDecimal start, BigDecimal end);

    @Query("SELECT article from Article article WHERE article.price between :start and :end")
    List<Article> findByPriceBetweenCustom(@Param("start") BigDecimal start, @Param("end") BigDecimal end);

    @Query("select article FROM Article article where article.price <= :priceAfter")
    List<Article> findByPriceAfter(@Param("priceAfter") BigDecimal priceAfter);

    @Query("select  article FROM Article article where article.price > :priceBefore")
    List<Article> findByPriceBefore(@Param("priceBefore") BigDecimal priceBefore);
}
