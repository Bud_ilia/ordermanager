package com.testproject.repositories;

import com.testproject.entities.ContentOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentOrderRepository extends JpaRepository<ContentOrder, Long>,
        QuerydslPredicateExecutor<ContentOrder> {

    @Query("select contentOrder FROM ContentOrder contentOrder where contentOrder.count between :start and :end")
    List<ContentOrder> findByCountBetween(@Param("start") Integer start, @Param("end") Integer end);

    @Query("select contentOrder FROM ContentOrder contentOrder where contentOrder.count <= :countAfter")
    List<ContentOrder> findByCountAfter(@Param("countAfter") Integer countAfter);

    @Query("select contentOrder FROM ContentOrder contentOrder where contentOrder.count <= :countBefore")
    List<ContentOrder> findByCountBefore(@Param("countBefore") Integer countBefore);
}
