package com.testproject.repositories

import com.testproject.entities.Article
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import spock.lang.Specification

@DataJpaTest
class ArticleRepositoryTest extends Specification {
    @Autowired
    ArticleRepository articleRepository

    def "save entity"() {
        given:
        def article = articleRepository.save(Article.builder()
                .name("item-1")
                .price(12 as BigDecimal)
                .build())

        expect:
        articleRepository.findById(article.getId()).isPresent()
    }

    def "delete entity"() {
        given:
        def article = articleRepository.save(Article.builder()
                .name("item-1")
                .price(12 as BigDecimal)
                .build())
        articleRepository.delete(article)
        expect:
        !articleRepository.findById(article.getId()).isPresent()
    }

    def "put entity"() {
        given:
        def article = articleRepository.save(Article.builder()
                .name("item-1")
                .price(12 as BigDecimal)
                .build())
        article.setPrice(17 as BigDecimal)
        articleRepository.save(article)
        expect:
        articleRepository.findById(article.getId()).get().getPrice() == (17 as BigDecimal)
    }

    def "Price between"() {
        given:
        articleRepository.save(Article.builder()
                .name("item-1")
                .price(11 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-5")
                .price(12 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-2")
                .price(13 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-3")
                .price(14 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-4")
                .price(15 as BigDecimal)
                .build())
        when:
        def articles = articleRepository.findByPriceBetween(12 as BigDecimal, 14 as BigDecimal)
        def articlesCustomQuery = articleRepository.findByPriceBetweenCustom(12 as BigDecimal, 14 as BigDecimal)

        then:
        articles.size() == 3
        articlesCustomQuery.size() == 3
    }

    def "Price after"() {
        given:
        articleRepository.save(Article.builder()
                .name("item-1")
                .price(11 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-5")
                .price(12 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-2")
                .price(13 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-3")
                .price(14 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-4")
                .price(15 as BigDecimal)
                .build())
        when:
        def articlesCustomQuery = articleRepository.findByPriceAfter(13 as BigDecimal)

        then:
        articlesCustomQuery.size() == 3
    }

    def "Price before"() {
        given:
        articleRepository.save(Article.builder()
                .name("item-1")
                .price(11 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-5")
                .price(12 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-2")
                .price(13 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-3")
                .price(14 as BigDecimal)
                .build())
        articleRepository.save(Article.builder()
                .name("item-4")
                .price(15 as BigDecimal)
                .build())
        when:
        def articlesCustomQuery = articleRepository.findByPriceBefore(13 as BigDecimal)

        then:
        articlesCustomQuery.size() == 2
    }

}
