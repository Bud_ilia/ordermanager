package com.testproject.repositories

import com.testproject.entities.Article
import com.testproject.entities.ContentOrder
import com.testproject.entities.Ordering
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import spock.lang.Specification

import java.time.LocalDateTime

@DataJpaTest
class ContentOrderRepositoryTest extends Specification {

    @Autowired
    ArticleRepository articleRepository

    @Autowired
    ContentOrderRepository contentRepository

    @Autowired
    OrderRepository orderRepository

    def "test between"() {
        given:

        def articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        def orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(13)
                .cost(12 as BigDecimal)
                .build())
        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(15)
                .cost(12 as BigDecimal)
                .build())

        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(17)
                .cost(12 as BigDecimal)
                .build())

        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(19)
                .cost(12 as BigDecimal)
                .build())
        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(21)
                .cost(12 as BigDecimal)
                .build())

        when:
        def contentOrders = contentRepository.findByCountBetween(14, 20)

        then:
        contentOrders.size() == 3
    }

    def "test before"() {
        given:

        def articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        def orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(13)
                .cost(12 as BigDecimal)
                .build())
        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(15)
                .cost(12 as BigDecimal)
                .build())

        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(17)
                .cost(12 as BigDecimal)
                .build())

        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(19)
                .cost(12 as BigDecimal)
                .build())
        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(21)
                .cost(12 as BigDecimal)
                .build())

        when:
        def contentOrders = contentRepository.findByCountBefore(16)

        then:
        contentOrders.size() == 2
    }

    def "test after"() {
        given:

        def articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        def orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(13)
                .cost(12 as BigDecimal)
                .build())
        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(15)
                .cost(12 as BigDecimal)
                .build())

        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(17)
                .cost(12 as BigDecimal)
                .build())

        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(19)
                .cost(12 as BigDecimal)
                .build())
        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(21)
                .cost(12 as BigDecimal)
                .build())

        when:
        def contentOrders = contentRepository.findByCountAfter(14)

        then:
        contentOrders.size() == 1
    }
}
