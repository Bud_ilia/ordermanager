package com.testproject.repositories

import com.testproject.entities.Article
import com.testproject.entities.ContentOrder
import com.testproject.entities.Ordering
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import spock.lang.Specification

import java.time.LocalDateTime

@DataJpaTest
class OrderRepositoryTest extends Specification {

    @Autowired
    ArticleRepository articleRepository

    @Autowired
    ContentOrderRepository contentRepository

    @Autowired
    OrderRepository orderRepository

    def "create record"() {
        given:

        def articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        def orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        def contentRec = contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(13)
                .cost(12 as BigDecimal)
                .build())
        expect:
        articleRepository.findById(articleRec.getId()).isPresent()
        contentRepository.findById(contentRec.getId()).isPresent()
        orderRepository.findById(orderingRec.getId()).isPresent()
    }

    def "delete record"() {
        given:

        def articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        def orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        def contentRec = contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(13)
                .cost(12 as BigDecimal)
                .build())
        articleRepository.deleteById(articleRec.getId())
        orderRepository.deleteById(orderingRec.getId())
        contentRepository.deleteById(contentRec.getId())

        expect:
        !articleRepository.findById(articleRec.getId()).isPresent()
        !contentRepository.findById(contentRec.getId()).isPresent()
        !orderRepository.findById(orderingRec.getId()).isPresent()
    }

    def "update record"() {
        given:

        def articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        def orderingRec = orderRepository.save(Ordering.builder()
                .email("sfsdffds@mail.ru")
                .time(LocalDateTime.MIN)
                .build())
        def contentRec = contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(13)
                .cost(12 as BigDecimal)
                .build())
        when:
        contentRec.setCount(19)
        contentRec.setCost(49 as BigDecimal)
        contentRepository.save(contentRec)
        orderingRec.setEmail("bud_ilia@mail.ru")
        orderRepository.save(orderingRec)
        then:
        contentRepository.findById(contentRec.getId()).get().getCost() == (49 as BigDecimal)
        contentRepository.findById(contentRec.getId()).get().getCount() == (19 as Integer)
        orderRepository.findById(orderingRec.getId()).get().getEmail().equals("bud_ilia@mail.ru")
    }

    def "find by email"() {
        given:

        def articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        def orderingRec = orderRepository.save(Ordering.builder()
                .email("bud_ilia@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(13)
                .cost(12 as BigDecimal)
                .build())

        articleRec = articleRepository.save(Article.builder()
                .price(12 as BigDecimal)
                .name("SDFSDF")
                .build()
        )
        orderingRec = orderRepository.save(Ordering.builder()
                .email("budilia@mail.ru")
                .time(LocalDateTime.MIN)
                .build())

        contentRepository.save(ContentOrder.builder()
                .article(articleRec)
                .ordering(orderingRec)
                .count(13)
                .cost(12 as BigDecimal)
                .build())
        when:
        def orderings = orderRepository.findByEmail("bud_ilia@mail.ru")
        then:
        orderings.size() == 1
    }
}
