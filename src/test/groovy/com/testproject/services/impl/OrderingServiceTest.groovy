package com.testproject.services.impl

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer
import com.testproject.dto.OrderDTO
import com.testproject.dto.jackson_wrappers.OrderingsWrapper
import spock.lang.Specification

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class OrderingServiceTest extends Specification {
    def "Read"() {
        given:
        JavaTimeModule module = new JavaTimeModule()
        module.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ISO_DATE_TIME))
        module.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ISO_DATE_TIME))

        ObjectMapper xmlMapper = new XmlMapper().registerModule(module)
        def wrapper = new OrderingsWrapper([OrderDTO.builder().time(LocalDateTime.now()).build()])
        String xml = xmlMapper.writeValueAsString(wrapper)
        expect:
        OrderingsWrapper readValue = xmlMapper.readValue(xml.getBytes(), OrderingsWrapper.class)
        readValue.orderings.get(0).time
    }
}
