package com.testproject.controllers

import com.testproject.dto.ArticleDTO
import com.testproject.services.ArticlesService
import groovy.json.JsonOutput
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import spock.lang.Specification

@WebMvcTest(controllers = [ArticleController])
class ArticleControllerTest extends Specification {
    @Autowired
    MockMvc mvc

    @SpringBean
    ArticlesService serviceArticle = Mock()

    def "test create"() {
        given:
        def articleDto = ArticleDTO.builder()
                .count(14 as Integer)
                .price(17 as BigDecimal)
                .build()
        when:
        def result = mvc
                .perform(MockMvcRequestBuilders.post("/api/article")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonOutput.toJson(articleDto as ArticleDTO)))
                .andReturn().response
        then:
        serviceArticle.create(_ as ArticleDTO) >> articleDto
        result.status == HttpStatus.CREATED.value()
    }

    def "test update"() {
        given:
        def articleDto = ArticleDTO.builder()
                .count(14 as Integer)
                .price(17 as BigDecimal)
                .build()
        serviceArticle.create(articleDto)
        when:
        def result = mvc
                .perform(MockMvcRequestBuilders.put("/api/article/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonOutput.toJson(articleDto as ArticleDTO)))
                .andReturn().response
        then:
        serviceArticle.update(_ as ArticleDTO) >> articleDto
        result.status == HttpStatus.OK.value()
    }

    def "test delete record"() {
        given:
        def articleDto = ArticleDTO.builder()
                .count(14 as Integer)
                .price(17 as BigDecimal)
                .build()
        serviceArticle.create(articleDto)
        when:
        def result = mvc
                .perform(MockMvcRequestBuilders.delete("/api/article/deleted/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().response
        then:
        serviceArticle.delete(_ as Long) >> 1
        result.status == HttpStatus.NO_CONTENT.value()
    }

    def "test delete record not luck"() {
        when:
        def result = mvc
                .perform(MockMvcRequestBuilders.delete("/api/article/deleted/2")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().response
        then:
        result.status == HttpStatus.GONE.value()
    }

    def "test get by id"() {
        given:
        def articleDto = ArticleDTO.builder()
                .count(14 as Integer)
                .price(17 as BigDecimal)
                .build()
        serviceArticle.create(articleDto)
        when:
        def result = mvc
                .perform(MockMvcRequestBuilders.get("/api/article/element/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().response
        then:
        1*serviceArticle.getById(1)
        result.status == HttpStatus.OK.value()
    }

    def "test get all"() {
        when:
        def result = mvc
                .perform(MockMvcRequestBuilders.get("/api/article/list")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().response
        then:
        1*serviceArticle.getAll()
        result.status == HttpStatus.OK.value()
    }

    def "test file download"() {
        when:
        def result = mvc
                .perform(MockMvcRequestBuilders.get("/api/article/downloaded/ggghhf")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().response
        then:
        serviceArticle.write("C:\\Users\\Ilya\\ggghhf.xml")
        result.status == HttpStatus.OK.value()
    }
//TODO: надо сделать тест на грузку файла
//    def "test file upload"() {
//        given:
//
//        MultipartFile file = new CommonsMultipartFile (new DiskFileItem  ("ghj.xml",null,true,517,(new File("E:\\ghj.xml"))));
//        when:
//        def result = mvc
//                .perform(MockMvcRequestBuilders.post("/api/article/")
//                        .contentType(MediaType.APPLICATION_OCTET_STREAM_VALUE)
//                .content(file.bytes))
//                .andReturn().response
//        then:
//        result.status == HttpStatus.OK.value()
//    }

    def "test find price after"() {
        serviceArticle.create(ArticleDTO.builder()
                .count(14 as Integer)
                .price(17 as BigDecimal)
                .build())
        serviceArticle.create(ArticleDTO.builder()
                .count(15 as Integer)
                .price(18 as BigDecimal)
                .build())
        serviceArticle.create(ArticleDTO.builder()
                .count(14 as Integer)
                .price(19 as BigDecimal)
                .build())
        serviceArticle.create(ArticleDTO.builder()
                .count(14 as Integer)
                .price(20 as BigDecimal)
                .build())
        serviceArticle.create(ArticleDTO.builder()
                .count(14 as Integer)
                .price(21 as BigDecimal)
                .build())
        when:
        def result = mvc
                .perform(MockMvcRequestBuilders.get("/api/article//list-price-after/")
                        .param("priceAfter", 19 as String)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().response
        then:
        1*serviceArticle.getByPriceAfter(19 as BigDecimal)
        result.status == HttpStatus.OK.value()
    }
}
