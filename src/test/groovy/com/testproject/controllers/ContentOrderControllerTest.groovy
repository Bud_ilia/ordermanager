package com.testproject.controllers

import com.testproject.dto.ArticleDTO
import com.testproject.dto.ContentOrderDTO
import com.testproject.dto.OrderDTO
import com.testproject.services.ContentOrderService
import com.testproject.services.OrderingService
import groovy.json.JsonOutput
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import spock.lang.Specification

import java.time.LocalDateTime

@WebMvcTest(controllers = [ContentOrderController])
class ContentOrderControllerTest extends Specification {
    @Autowired
    MockMvc mvc

    @SpringBean
    ContentOrderService serviceContent = Mock()

    @SpringBean
    OrderingService serviceOrdering = Mock()

    def "test create"() {
        given:
        def articleDto = ArticleDTO.builder()
                .count(14 as Integer)
                .price(17 as BigDecimal)
                .build()
        def contentDto = ContentOrderDTO.builder()
                .count(19)
                .article(articleDto)
                .cost(33 as BigDecimal)
                .build()
        List<ContentOrderDTO> list = new ArrayList<>()
        list.add(contentDto)
        def orderingDto = OrderDTO.builder()
                .email("dsfd.ru")
//                .time(LocalDateTime.now())
                .contentOrders(list)
                .build()
        serviceOrdering.create(orderingDto)
        when:

        def result = mvc
                .perform(MockMvcRequestBuilders.put("/api/content-order/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonOutput.toJson(contentDto as ContentOrderDTO)))
                .andReturn().response
        then:
        serviceContent.update(_ as ContentOrderDTO) >> contentDto
        result.status == HttpStatus.OK.value()
    }

    def "test delete record"() {
        when:
        def result = mvc
                .perform(MockMvcRequestBuilders.delete("/api/content-order/deleted/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().response
        then:
        serviceContent.delete(_ as Long) >> 1
        result.status == HttpStatus.NO_CONTENT.value()
    }
}
