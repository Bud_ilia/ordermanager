package com.testproject.mapper

import com.testproject.dto.ContentOrderDTO
import com.testproject.entities.Article
import com.testproject.entities.ContentOrder
import com.testproject.entities.Ordering
import org.mapstruct.factory.Mappers
import spock.lang.Specification


class ContentOrderMapperTest extends Specification {
    def "Update"() {
        given:
        def mapper = Mappers.getMapper(ContentOrderMapper.class)

        def article = Article.builder()
                .id(9)
                .name("item1")
                .price(5 as BigDecimal)
                .build()

        def ordering = Ordering.builder()
                .id(3)
                .email("bud@mail.ru")
                .build()

        def entity = ContentOrder.builder()
                .id(666)
                .ordering(ordering)
                .article(article)
                .count(5)
                .cost(10)
                .build()

        def dto = ContentOrderDTO.builder()
                .cost(100)
                .build()

        when:
        def result = mapper.update(dto, entity)

        then:
        result.id == 666
        result.cost == (BigDecimal) 100
        result.count == 5
    }
}
