package com.testproject.mapper

import com.testproject.dto.ArticleDTO
import com.testproject.entities.Article
import org.mapstruct.factory.Mappers
import spock.lang.Specification


class ArticleMapperTest extends Specification {
    def "Update"() {
        given:
        def mapper = Mappers.getMapper(ArticleMapper.class)

        def entity = Article.builder()
                .id(4)
                .name("item1")
                .price(8 as BigDecimal)
                .build()

        def dto = ArticleDTO.builder()
                .name("Milk")
                .price(4 as BigDecimal)
                .build()
        when:
        def result = mapper.update(dto, entity)

        then:
        result.id == 4
        result.name == "Milk"
        result.price == 4

    }
}
