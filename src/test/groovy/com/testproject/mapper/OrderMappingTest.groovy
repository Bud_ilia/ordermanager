package com.testproject.mapper

import com.testproject.dto.OrderDTO
import com.testproject.entities.Article
import com.testproject.entities.ContentOrder
import com.testproject.entities.Ordering
import org.mapstruct.factory.Mappers
import spock.lang.Specification


class OrderMappingTest extends Specification {
    def "Update"() {
        given:
        def mapper = Mappers.getMapper(OrderMapping.class)

        def article = Article.builder()
                .id(9)
                .name("item1")
                .price(5 as BigDecimal)
                .build()

        def entity = Ordering.builder()
                .id(354)
                .email("bud@mail.ru")
                .build()

        def contentOrder = ContentOrder.builder()
                .id(666)
                .ordering(entity)
                .article(article)
                .count(5)
                .cost(10)
                .build()

        def dto = OrderDTO.builder()
                .id(5)
                .email("Ready@mail.ru")
                .build()


        when:
        def result = mapper.update(dto, entity)

        then:
        result.email == "Ready@mail.ru"
        result.id == 5

    }
}
