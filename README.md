OrderManager
===

Основные сведения
----

Сервис разрабатывался в учебных целях. Сервис предназначен для управления заказами.

Разработчик Гусев Илья. Электронная почта: bud_ilia@mail.ru.
***

Стек технологий

  * **Java SE:** Core, Collections, IO;
  
  * **Backend:** Spring Framework(MVC), Spring Boot, Spring Data(JPA, JDBC), HIbernate, Lombok, Liquibase, H2, QueryDsl, MapStruct, JDOM, Jackson;
  
  * **Тестирование:** Swagger, Groovy, Mockito;
  
  * **Сервер:** Tomcat;
  
  * **Система контроля версий:** Git;
  
  * **Инструменты:** Maven.
  
  ***
  
Плагины, которые могут потребоваться для компиляции проекта

  * Lombok
  * MapStruct support
  * Log4j Plugin 
  
***

Цели проекта
---

  * разработать REST сервис с API;
  * изучить различные технологии.
  
***

Технические требования
---

  * Операционная система: Linux Ubuntu/ Windows 10;
  * JDK 1.8.
  
***
